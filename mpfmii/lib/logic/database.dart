import 'package:cloud_firestore/cloud_firestore.dart';

final FirebaseFirestore _firestore = FirebaseFirestore.instance;
final CollectionReference _mainCollection = _firestore.collection('appmmpf');

class Database {
  static String userUid;

  ///------------- Income
  static Future<void> addInCome({
    String category,
    String description,
    DateTime dateOfPay,
    double amount,
  }) async {
    DocumentReference documentReferencer =
        _mainCollection.doc(userUid).collection('incomes').doc();

    Map<String, dynamic> data = <String, dynamic>{
      "category": category,
      "description": description,
      "dateOfPay": dateOfPay,
      "amount": amount,
    };

    await documentReferencer
        .set(data)
        .whenComplete(() => print("incomes item added to the database"))
        .catchError((e) => print(e));
  }

  static Future<void> updateIncome({
    String category,
    String description,
    DateTime dateOfPay,
    double amount,
    String docId,
  }) async {
    DocumentReference documentReferencer =
        _mainCollection.doc(userUid).collection('incomes').doc(docId);

    Map<String, dynamic> data = <String, dynamic>{
      "category": category,
      "description": description,
      "dateOfPay": dateOfPay,
      "amount": amount,
    };

    await documentReferencer
        .update(data)
        .whenComplete(() => print("incomes item updated in the database"))
        .catchError((e) => print(e));
  }

  static Stream<QuerySnapshot> readInCome() {
    CollectionReference notesItemCollection =
        _mainCollection.doc(userUid).collection('incomes');

    return notesItemCollection.snapshots();
  }

  static Future<void> deleteInCome({
    String docId,
  }) async {
    DocumentReference documentReferencer =
        _mainCollection.doc(userUid).collection('incomes').doc(docId);

    await documentReferencer
        .delete()
        .whenComplete(() => print('incomes item deleted from the database'))
        .catchError((e) => print(e));
  }

  ///-------------- Spending
  static Future<void> addSpending({
    String category,
    String description,
    DateTime dateOfPay,
    double amount,
  }) async {
    DocumentReference documentReferencer =
        _mainCollection.doc(userUid).collection('spending').doc();

    Map<String, dynamic> data = <String, dynamic>{
      "category": category,
      "description": description,
      "dateOfPay": dateOfPay,
      "amount": amount,
    };

    await documentReferencer
        .set(data)
        .whenComplete(() => print("Spending item added to the database"))
        .catchError((e) => print(e));
  }

  static Future<void> updateSpending({
    String category,
    String description,
    DateTime dateOfPay,
    double amount,
    String docId,
  }) async {
    DocumentReference documentReferencer =
        _mainCollection.doc(userUid).collection('spending').doc(docId);

    Map<String, dynamic> data = <String, dynamic>{
      "category": category,
      "description": description,
      "dateOfPay": dateOfPay,
      "amount": amount,
    };

    await documentReferencer
        .update(data)
        .whenComplete(() => print("Spending item updated in the database"))
        .catchError((e) => print(e));
  }

  static Stream<QuerySnapshot> readSpending() {
    CollectionReference notesItemCollection =
        _mainCollection.doc(userUid).collection('spending');

    return notesItemCollection.snapshots();
  }

  static Future<void> deleteSpending({
    String docId,
  }) async {
    DocumentReference documentReferencer =
        _mainCollection.doc(userUid).collection('spending').doc(docId);

    await documentReferencer
        .delete()
        .whenComplete(() => print('Spending item deleted from the database'))
        .catchError((e) => print(e));
  }

  ///------------------------------ User

  static Future<void> addUser({
    String userName,
    String password,
    String fullName,
    DateTime dateOfBirth,
    String gender,
    double limit,
  }) async {
    DocumentReference documentReferencer = _mainCollection.doc();
    Map<String, dynamic> data = <String, dynamic>{
      "user_name": userName,
      "password": password,
      "full_name": fullName,
      "dateOfBirth": dateOfBirth,
      "gender": gender,
      "limit": limit,
    };

    await documentReferencer
        .set(data)
        .whenComplete(() => print("User infor added to the database"))
        .catchError((e) => print(e));
  }

  static Future<void> updateUser({
    String userName,
    String password,
    String fullName,
    DateTime dateOfBirth,
    String gender,
    double limit,
    String docId,
  }) async {
    DocumentReference documentReferencer = _mainCollection.doc(docId);

    Map<String, dynamic> data = <String, dynamic>{
      "user_name": userName,
      "password": password,
      "full_name": fullName,
      "dateOfBirth": dateOfBirth,
      "gender": gender,
      "limit": limit,
    };

    await documentReferencer
        .update(data)
        .whenComplete(() => print("User infor updated in the database"))
        .catchError((e) => print(e));
  }

  static Stream<QuerySnapshot> readUser() {
    CollectionReference notesItemCollection = _mainCollection;

    return notesItemCollection.snapshots();
  }

  static Future<void> deleteUser({
    String docId,
  }) async {
    DocumentReference documentReferencer = _mainCollection.doc(docId);

    await documentReferencer
        .delete()
        .whenComplete(() => print('User infor deleted from the database'))
        .catchError((e) => print(e));
  }
}
