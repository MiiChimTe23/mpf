import 'package:flutter/material.dart';
import 'package:mpfmii/constants.dart';
import 'package:mpfmii/screens/Report/componnents/body_report.dart';

class ReportSrceen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "THỐNG KÊ TIÊU DÙNG CÁ NHÂN",
          style: TextStyle(color: CustomColors.MainLightColor),
        ),
        centerTitle: true,
        backgroundColor: CustomColors.MainColor,
      ),
      body: DashboardPage(),
    );
  }
}
