import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:mpfmii/constants.dart';
import 'package:mpfmii/screens/Spending/componnents/list_view_income.dart';
import 'package:mpfmii/screens/Spending/componnents/list_view_spending.dart';

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  @override
  Widget build(BuildContext context) {
    var data = [
      ClickPerMonth('1', 30, CustomColors.MPFYellow),
      ClickPerMonth('2', 100, CustomColors.MPFRed),
      ClickPerMonth('3', 54, CustomColors.MPFPink),
      ClickPerMonth('4', 20, CustomColors.MPFBlue),
      ClickPerMonth('5', 81, CustomColors.MPFGreen),
      ClickPerMonth('6', 24, CustomColors.MPFLightBlue),
      ClickPerMonth('7', 12, CustomColors.MPFYellow),
      ClickPerMonth('8', 23, CustomColors.MPFRed),
      ClickPerMonth('9', 5, CustomColors.MPFPurple),
      ClickPerMonth('10', 34, CustomColors.MainColor),
      ClickPerMonth('11', 65, CustomColors.MPFGreen),
      ClickPerMonth('12', 22, CustomColors.MPFLightBlue),
    ];

    var series = [
      new charts.Series(
          id: 'Clicks',
          domainFn: (ClickPerMonth clickData, _) => clickData.month,
          measureFn: (ClickPerMonth clickData, _) => clickData.clicks,
          colorFn: (ClickPerMonth clickData, _) => clickData.color,
          data: data)
    ];

    var chart = new charts.BarChart(series,
        animate: true, animationDuration: Duration(milliseconds: 1500));

    var chartWidget = Padding(
      padding: EdgeInsets.all(20.0),
      child: SizedBox(height: 200.0, child: chart),
    );
    
    return Scaffold(
      backgroundColor: CustomColors.MainColor,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 20),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 25.0),
                child: Container(
                  width: double.infinity,
                  height: 350.0,
                  decoration: BoxDecoration(
                      color: CustomColors.MainLightColor,
                      borderRadius: BorderRadius.all(Radius.circular(20.0)),
                      boxShadow: [
                        BoxShadow(
                            color: CustomColors.MainLightColor.withOpacity(0.1),
                            offset: Offset(0.0, 0.3),
                            blurRadius: 15.0)
                      ],),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 25.0, vertical: 25.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Text(
                                  '4,000,000',
                                  style: TextStyle(
                                      color: CustomColors.BackgroundColor,
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(height: 5.0),
                                Text(
                                  'Giới hạn chi tiêu',
                                  style: TextStyle(
                                    color: CustomColors.BackgroundColor,
                                    fontSize: 14.0,
                                  ),
                                )
                              ],
                            ),
                            IconButton(
                              icon: Icon(Icons.show_chart),
                              onPressed: () {},
                              color: CustomColors.MainColor,
                              iconSize: 30.0,
                            )
                          ],
                        ),
                      ),
                      chartWidget
                    ],
                  ),
                ),
              ),
              Divider(
                color: CustomColors.MainLightColor.withOpacity(0.4),
                height: 20,
                thickness: 3,
                indent: 60,
                endIndent: 60,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Material(
                          borderRadius: BorderRadius.circular(5.0),
                          color: CustomColors.BackgroundColor,
                          child: IconButton(
                            padding: EdgeInsets.all(5.0),
                            icon: Icon(Icons.upload_sharp),
                            color: CustomColors.MainLightColor,
                            iconSize: 20.0,
                            onPressed: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      ListViewSpending()));
                            },
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text('Xem chi tiết khoản chi',
                            style: TextStyle(
                                color: CustomColors.BackgroundColor,
                                fontWeight: FontWeight.bold,
                                fontSize: 15))
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Material(
                          borderRadius: BorderRadius.circular(5.0),
                          color: CustomColors.BackgroundColor,
                          child: IconButton(
                            padding: EdgeInsets.all(5.0),
                            icon: Icon(Icons.download_sharp),
                            color: CustomColors.MainLightColor,
                            iconSize: 20.0,
                            onPressed: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      ListViewInCome()));
                            },
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text('Xem chi tiết khoản thu',
                            style: TextStyle(
                                color: CustomColors.BackgroundColor,
                                fontWeight: FontWeight.bold,
                                fontSize: 15))
                      ],
                    ),
                  ],
                ),
              ),
              Divider(
                color: CustomColors.MainLightColor.withOpacity(0.4),
                height: 20,
                thickness: 3,
                indent: 60,
                endIndent: 60,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 10.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Icon(
                      Icons.bar_chart,
                      color: CustomColors.BackgroundColor,
                      size: 25.0,
                    ),
                    Icon(
                      Icons.bubble_chart,
                      color: CustomColors.BackgroundColor,
                      size: 25.0,
                    ),
                    Icon(
                      Icons.pie_chart,
                      color: CustomColors.BackgroundColor,
                      size: 25.0,
                    ),
                    Text(
                      'CHI TIÊU GẦN ĐÂY',
                      style: TextStyle(
                          color: CustomColors.MainLightColor,
                          fontWeight: FontWeight.bold,
                          fontSize: 18),
                    ),
                    Icon(
                      Icons.pie_chart,
                      color: CustomColors.BackgroundColor,
                      size: 25.0,
                    ),
                    Icon(
                      Icons.bubble_chart,
                      color: CustomColors.BackgroundColor,
                      size: 25.0,
                    ),
                    Icon(
                      Icons.bar_chart,
                      color: CustomColors.BackgroundColor,
                      size: 25.0,
                    ),
                  ],
                ),
              ),
              SizedBox(height: 5),
              Padding(
                padding: EdgeInsets.all(25.0),
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: CustomColors.BackgroundColor,
                    borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  ),
                  child: Column(
                    children: [
                      Padding(
                        padding:
                        const EdgeInsets.all(20.0),
                        child: Row(
                          children: <Widget>[
                            Material(
                              borderRadius: BorderRadius.circular(100.0),
                              color: CustomColors.MainLightColor,
                              child: Padding(
                                padding: EdgeInsets.all(15.0),
                                child: Icon(
                                  Icons.fastfood,
                                  size: 20.0,
                                  color: CustomColors.MPFPurple,
                                ),
                              ),
                            ),
                            SizedBox(width: 15.0),
                            Expanded(
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Ăn sáng với người yêu',
                                    style: TextStyle(
                                        color: CustomColors.MainLightColor,
                                        fontSize: 15.0),
                                  ),
                                  Text(
                                    '20/06/2021',
                                    style: TextStyle(
                                        color: CustomColors.MainLightColor,
                                        fontSize: 12.0),
                                  )
                                ],
                              ),
                            ),
                            Text(
                              '- 45,000',
                              style: TextStyle(
                                  color: CustomColors.MainLightColor,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding:
                        const EdgeInsets.all(20.0),
                        child: Row(
                          children: <Widget>[
                            Material(
                              borderRadius: BorderRadius.circular(100.0),
                              color: CustomColors.MainLightColor,
                              child: Padding(
                                padding: EdgeInsets.all(15.0),
                                child: Icon(
                                  Icons.accessibility_new,
                                  size: 20.0,
                                  color: CustomColors.MPFBlue,
                                ),
                              ),
                            ),
                            SizedBox(width: 15.0),
                            Expanded(
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Tiền trọ tháng 5',
                                    style: TextStyle(
                                        color: CustomColors.MainLightColor,
                                        fontSize: 15.0),
                                  ),
                                  Text(
                                    '12/05/2021',
                                    style: TextStyle(
                                        color: CustomColors.MainLightColor,
                                        fontSize: 12.0),
                                  )
                                ],
                              ),
                            ),
                            Text(
                              '- 800,000',
                              style: TextStyle(
                                  color: CustomColors.MainLightColor,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding:
                        const EdgeInsets.all(20.0),
                        child: Row(
                          children: <Widget>[
                            Material(
                              borderRadius: BorderRadius.circular(100.0),
                              color: CustomColors.MainLightColor,
                              child: Padding(
                                padding: EdgeInsets.all(15.0),
                                child: Icon(
                                  Icons.fastfood,
                                  size: 20.0,
                                  color: CustomColors.MPFPurple,
                                ),
                              ),
                            ),
                            SizedBox(width: 15.0),
                            Expanded(
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Ăn sáng với người yêu',
                                    style: TextStyle(
                                        color: CustomColors.MainLightColor,
                                        fontSize: 15.0),
                                  ),
                                  Text(
                                    '20/06/2021',
                                    style: TextStyle(
                                        color: CustomColors.MainLightColor,
                                        fontSize: 12.0),
                                  )
                                ],
                              ),
                            ),
                            Text(
                              '- 45,000',
                              style: TextStyle(
                                  color: CustomColors.MainLightColor,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding:
                        const EdgeInsets.all(20.0),
                        child: Row(
                          children: <Widget>[
                            Material(
                              borderRadius: BorderRadius.circular(100.0),
                              color: CustomColors.MainLightColor,
                              child: Padding(
                                padding: EdgeInsets.all(15.0),
                                child: Icon(
                                  Icons.accessibility_new,
                                  size: 20.0,
                                  color: CustomColors.MPFBlue,
                                ),
                              ),
                            ),
                            SizedBox(width: 15.0),
                            Expanded(
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Tiền trọ tháng 5',
                                    style: TextStyle(
                                        color: CustomColors.MainLightColor,
                                        fontSize: 15.0),
                                  ),
                                  Text(
                                    '12/05/2021',
                                    style: TextStyle(
                                        color: CustomColors.MainLightColor,
                                        fontSize: 12.0),
                                  )
                                ],
                              ),
                            ),
                            Text(
                              '- 800,000',
                              style: TextStyle(
                                  color: CustomColors.MainLightColor,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding:
                        const EdgeInsets.all(20.0),
                        child: Row(
                          children: <Widget>[
                            Material(
                              borderRadius: BorderRadius.circular(100.0),
                              color: CustomColors.MainLightColor,
                              child: Padding(
                                padding: EdgeInsets.all(15.0),
                                child: Icon(
                                  Icons.fastfood,
                                  size: 20.0,
                                  color: CustomColors.MPFPurple,
                                ),
                              ),
                            ),
                            SizedBox(width: 15.0),
                            Expanded(
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Ăn sáng với người yêu',
                                    style: TextStyle(
                                        color: CustomColors.MainLightColor,
                                        fontSize: 15.0),
                                  ),
                                  Text(
                                    '20/06/2021',
                                    style: TextStyle(
                                        color: CustomColors.MainLightColor,
                                        fontSize: 12.0),
                                  )
                                ],
                              ),
                            ),
                            Text(
                              '- 45,000',
                              style: TextStyle(
                                  color: CustomColors.MainLightColor,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding:
                        const EdgeInsets.all(20.0),
                        child: Row(
                          children: <Widget>[
                            Material(
                              borderRadius: BorderRadius.circular(100.0),
                              color: CustomColors.MainLightColor,
                              child: Padding(
                                padding: EdgeInsets.all(15.0),
                                child: Icon(
                                  Icons.accessibility_new,
                                  size: 20.0,
                                  color: CustomColors.MPFBlue,
                                ),
                              ),
                            ),
                            SizedBox(width: 15.0),
                            Expanded(
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Tiền trọ tháng 5',
                                    style: TextStyle(
                                        color: CustomColors.MainLightColor,
                                        fontSize: 15.0),
                                  ),
                                  Text(
                                    '12/05/2021',
                                    style: TextStyle(
                                        color: CustomColors.MainLightColor,
                                        fontSize: 12.0),
                                  )
                                ],
                              ),
                            ),
                            Text(
                              '- 800,000',
                              style: TextStyle(
                                  color: CustomColors.MainLightColor,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding:
                        const EdgeInsets.all(20.0),
                        child: Row(
                          children: <Widget>[
                            Material(
                              borderRadius: BorderRadius.circular(100.0),
                              color: CustomColors.MainLightColor,
                              child: Padding(
                                padding: EdgeInsets.all(15.0),
                                child: Icon(
                                  Icons.fastfood,
                                  size: 20.0,
                                  color: CustomColors.MPFPurple,
                                ),
                              ),
                            ),
                            SizedBox(width: 15.0),
                            Expanded(
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Ăn sáng với người yêu',
                                    style: TextStyle(
                                        color: CustomColors.MainLightColor,
                                        fontSize: 15.0),
                                  ),
                                  Text(
                                    '20/06/2021',
                                    style: TextStyle(
                                        color: CustomColors.MainLightColor,
                                        fontSize: 12.0),
                                  )
                                ],
                              ),
                            ),
                            Text(
                              '- 45,000',
                              style: TextStyle(
                                  color: CustomColors.MainLightColor,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}

class ClickPerMonth {
  final String month;
  final int clicks;
  final charts.Color color;

  ClickPerMonth(this.month, this.clicks, Color color)
      : this.color = new charts.Color(
            r: color.red, g: color.green, b: color.blue, a: color.alpha);
}
