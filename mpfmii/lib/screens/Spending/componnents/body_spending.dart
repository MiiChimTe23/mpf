import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mpfmii/constants.dart';
import 'package:mpfmii/screens/Spending/componnents/add_spending_khoanchi.dart';
import 'package:mpfmii/screens/Spending/componnents/add_spending_khoanthu.dart';

class BodySpD extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context)
        .size; //bien"size" cung cap chieu cao va chieu rong cua app
    double titleSize = 15;
    //double contentSize = 13;

    return Scaffold(
      backgroundColor: CustomColors.BackgroundColor,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                width: double.infinity,
                decoration: BoxDecoration(
                    color: CustomColors.MainColor,
                    border: Border.all(color: CustomColors.MainColor)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                        icon: Icon(
                          Icons.arrow_back_ios,
                          color: Colors.white,
                          size: 25.0,
                        ),
                        onPressed: () {}),
                    Text(
                      'Tháng 6/2021',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    IconButton(
                        icon: Icon(
                          Icons.arrow_forward_ios,
                          color: Colors.white,
                          size: 25.0,
                        ),
                        onPressed: () {}),
                  ],
                ),
              ),
              Stack(
                children: <Widget>[
                  ClipPath(
                    clipper: CustomShapeClipper(),
                    child: Container(
                      height: 300.0,
                      decoration: BoxDecoration(color: CustomColors.MainColor),
                    ),
                  ),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 25.0, vertical: 20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Số dư khả dụng',
                              style: TextStyle(
                                  color: Colors.white, fontSize: titleSize),
                            ),
                            SizedBox(height: size.height * 0.01),
                            Text(
                              '23,200,000',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Material(
                              borderRadius: BorderRadius.circular(15.0),
                              color: CustomColors.BackgroundColor,
                              child: IconButton(
                                padding: EdgeInsets.all(10.0),
                                icon: Icon(Icons.attach_money),
                                color: CustomColors.MainColor,
                                iconSize: 30.0,
                                onPressed: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          ADDSpendingKT()));
                                },
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text('Thêm khoản thu',
                                style: TextStyle(
                                    color: CustomColors.BackgroundColor,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15))
                          ],
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                        EdgeInsets.only(top: 130.0, right: 25.0, left: 25.0),
                    child: Container(
                      width: double.infinity,
                      height: 300.0,
                      decoration: BoxDecoration(
                          color: CustomColors.MainLightColor,
                          borderRadius: BorderRadius.all(Radius.circular(20.0)),
                          boxShadow: [
                            BoxShadow(
                                color: CustomColors.MPFYellow.withOpacity(0.1),
                                offset: Offset(0.0, 3.0),
                                blurRadius: 15.0)
                          ]),
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 40.0, vertical: 40.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Column(
                                  children: <Widget>[
                                    Material(
                                      borderRadius:
                                          BorderRadius.circular(100.0),
                                      color: CustomColors.MPFPurple.withOpacity(
                                          0.1),
                                      child: IconButton(
                                        padding: EdgeInsets.all(15.0),
                                        icon: Icon(Icons.fastfood),
                                        color: CustomColors.MPFPurple,
                                        iconSize: 30.0,
                                        onPressed: () {
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder:
                                                      (BuildContext context) =>
                                                          ADDSpending()));
                                        },
                                      ),
                                    ),
                                    SizedBox(height: 8.0),
                                    Text('Ăn uống',
                                        style: TextStyle(
                                            color: CustomColors.BackgroundColor,
                                            fontWeight: FontWeight.bold))
                                  ],
                                ),
                                Column(
                                  children: <Widget>[
                                    Material(
                                      borderRadius:
                                          BorderRadius.circular(100.0),
                                      color:
                                          CustomColors.MPFBlue.withOpacity(0.1),
                                      child: IconButton(
                                        padding: EdgeInsets.all(15.0),
                                        icon: Icon(Icons.accessibility_new),
                                        color: CustomColors.MPFBlue,
                                        iconSize: 30.0,
                                        onPressed: () {
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder:
                                                      (BuildContext context) =>
                                                          ADDSpending()));
                                        },
                                      ),
                                    ),
                                    SizedBox(height: 8.0),
                                    Text('Sinh hoạt',
                                        style: TextStyle(
                                            color: CustomColors.BackgroundColor,
                                            fontWeight: FontWeight.bold))
                                  ],
                                ),
                                Column(
                                  children: <Widget>[
                                    Material(
                                      borderRadius:
                                          BorderRadius.circular(100.0),
                                      color: CustomColors.MainColor.withOpacity(
                                          0.1),
                                      child: IconButton(
                                        padding: EdgeInsets.all(15.0),
                                        icon:
                                            Icon(Icons.airport_shuttle_rounded),
                                        color: CustomColors.MainColor,
                                        iconSize: 30.0,
                                        onPressed: () {
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder:
                                                      (BuildContext context) =>
                                                          ADDSpending()));
                                        },
                                      ),
                                    ),
                                    SizedBox(height: 8.0),
                                    Text('Di chuyển',
                                        style: TextStyle(
                                            color: CustomColors.BackgroundColor,
                                            fontWeight: FontWeight.bold))
                                  ],
                                )
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 40.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Column(
                                  children: <Widget>[
                                    Material(
                                      borderRadius:
                                          BorderRadius.circular(100.0),
                                      color: CustomColors.MPFYellow.withOpacity(
                                          0.1),
                                      child: IconButton(
                                        padding: EdgeInsets.all(15.0),
                                        icon: Icon(Icons.auto_awesome),
                                        color: CustomColors.MPFYellow,
                                        iconSize: 30.0,
                                        onPressed: () {
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder:
                                                      (BuildContext context) =>
                                                          ADDSpending()));
                                        },
                                      ),
                                    ),
                                    SizedBox(height: 8.0),
                                    Text('Phụ kiện',
                                        style: TextStyle(
                                            color: CustomColors.BackgroundColor,
                                            fontWeight: FontWeight.bold))
                                  ],
                                ),
                                Column(
                                  children: <Widget>[
                                    Material(
                                      borderRadius:
                                          BorderRadius.circular(100.0),
                                      color:
                                          CustomColors.MPFPink.withOpacity(0.1),
                                      child: IconButton(
                                        padding: EdgeInsets.all(15.0),
                                        icon: Icon(Icons.local_florist),
                                        color: CustomColors.MPFPink,
                                        iconSize: 30.0,
                                        onPressed: () {
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder:
                                                      (BuildContext context) =>
                                                          ADDSpending()));
                                        },
                                      ),
                                    ),
                                    SizedBox(height: 8.0),
                                    Text('Hiếu hỉ',
                                        style: TextStyle(
                                            color: CustomColors.BackgroundColor,
                                            fontWeight: FontWeight.bold))
                                  ],
                                ),
                                Column(
                                  children: <Widget>[
                                    Material(
                                      borderRadius:
                                          BorderRadius.circular(100.0),
                                      color:
                                          CustomColors.MPFRed.withOpacity(0.1),
                                      child: IconButton(
                                        padding: EdgeInsets.all(15.0),
                                        icon: Icon(
                                            Icons.account_balance_wallet_sharp),
                                        color: CustomColors.MPFRed,
                                        iconSize: 30.0,
                                        onPressed: () {
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder:
                                                      (BuildContext context) =>
                                                          ADDSpending()));
                                        },
                                      ),
                                    ),
                                    SizedBox(height: 8.0),
                                    Text('Khoản khác',
                                        style: TextStyle(
                                            color: CustomColors.BackgroundColor,
                                            fontWeight: FontWeight.bold))
                                  ],
                                )
                              ],
                            ),
                          ),
                          SizedBox(height: size.height * 0.05),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 25, vertical: 20),
                // ignore: deprecated_member_use
                child: FlatButton(
                  padding: EdgeInsets.all(15),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15)),
                  color: CustomColors.MainColor,
                  onPressed: () {},
                  child: Row(
                    children: [
                      SizedBox(width: 10),
                      Expanded(
                        child: Text(
                          'Số tiền đã sử dụng',
                          style: TextStyle(
                            color: CustomColors.MainLightColor.withOpacity(0.8),
                            fontSize: 15,
                          ),
                        ),
                      ),
                      Text(
                        '3,200,000',
                        style: TextStyle(
                            color: CustomColors.MainLightColor,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(width: 10),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 10.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Icon(
                      Icons.label_important,
                      color: CustomColors.MPFLightBlue,
                      size: 25.0,
                    ),
                    Icon(
                      Icons.label_important,
                      color: CustomColors.MPFLightBlue,
                      size: 25.0,
                    ),
                    Icon(
                      Icons.label_important,
                      color: CustomColors.MPFLightBlue,
                      size: 25.0,
                    ),
                    Text(
                      'CHI TIÊU DANH MỤC',
                      style: TextStyle(
                          color: CustomColors.MainLightColor,
                          fontWeight: FontWeight.bold,
                          fontSize: 18),
                    ),
                    Icon(
                      Icons.label_important,
                      color: CustomColors.MPFLightBlue,
                      size: 25.0,
                    ),
                    Icon(
                      Icons.label_important,
                      color: CustomColors.MPFLightBlue,
                      size: 25.0,
                    ),
                    Icon(
                      Icons.label_important,
                      color: CustomColors.MPFLightBlue,
                      size: 25.0,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 25.0),
                child: Container(
                  height: 100.0,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: <Widget>[
                      UpcomingCard(
                        title: 'Ăn uống',
                        value: "450,000",
                        color: CustomColors.MPFPurple,
                      ),
                      UpcomingCard(
                        title: 'Sinh hoạt',
                        value: '1,256,000',
                        color: CustomColors.MPFBlue,
                      ),
                      UpcomingCard(
                        title: 'Di chuyển',
                        value: '210,000',
                        color: CustomColors.MainColor,
                      ),
                      UpcomingCard(
                        title: 'Phụ kiện',
                        value: '679,000',
                        color: CustomColors.MPFYellow,
                      ),
                      UpcomingCard(
                        title: 'Hiếu hỉ',
                        value: '500,000',
                        color: CustomColors.MPFPink,
                      ),
                      UpcomingCard(
                        title: 'Danh mục khác',
                        value: '315,000',
                        color: CustomColors.MPFRed,
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class CustomShapeClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();

    path.lineTo(0.0, 390.0 - 200);
    path.quadraticBezierTo(size.width / 2, 280, size.width, 390.0 - 200);
    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}

class UpcomingCard extends StatelessWidget {
  final String title;
  final String value;
  final Color color;

  UpcomingCard({this.title, this.value, this.color});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: Container(
        width: 150.0,
        height: 100.0,
        decoration: BoxDecoration(
            color: color, borderRadius: BorderRadius.all(Radius.circular(4.0))),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(title,
                  style: TextStyle(
                    color: CustomColors.MainLightColor,
                  )),
              SizedBox(height: 15.0),
              Text('$value',
                  style: TextStyle(
                      fontSize: 18.0,
                      color: CustomColors.MainLightColor,
                      fontWeight: FontWeight.bold))
            ],
          ),
        ),
      ),
    );
  }
}
