import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mpfmii/constants.dart';

class ADDSpending extends StatefulWidget {
  @override
  _ADDSpendingState createState() => _ADDSpendingState();
}

class _ADDSpendingState extends State<ADDSpending> {
  final _formKey = GlobalKey<FormState>();

  String _danhMucChiTieu;
  String _moTaChiTieu = '';
  DateTime _ngayChiTieu = DateTime.now();
  // ignore: unused_field
  double _tongTien;

  final DateFormat _dateFormat = DateFormat('dd/MM/yyyy');
  final List<String> _danhMucCT = [
    'Ăn uống',
    'Sinh hoạt',
    'Di chuyển',
    'Phụ kiện',
    'Hiếu hỉ',
    'Khác'
  ];
  TextEditingController _ngayChiTieuText = TextEditingController();

  _handleDatePicker1() async {
    final DateTime date = await showDatePicker(
      context: context,
      initialDate: _ngayChiTieu,
      firstDate: DateTime(2000),
      lastDate: DateTime(2100),
    );
    if (date != null && date != _ngayChiTieu) {
      setState(() {
        _ngayChiTieu = date;
      });
      _ngayChiTieuText.text = _dateFormat.format(date);
    }
  }

  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            size: 30.0,
            color: CustomColors.MainColor,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Text(
          "THÊM KHOẢN CHI",
          style: TextStyle(color: CustomColors.MainColor),
        ),
        centerTitle: true,
        backgroundColor: CustomColors.BackgroundColor,
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: SingleChildScrollView(
          child: Container(
            width: double.infinity,
            height: size.height,
            color: CustomColors.BackgroundColor,
            padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 25.0),
            child: Column(
              children: <Widget>[
                Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 20),
                      Image.asset(
                        'assets/images/savings.png',
                        width: 150,
                        height: 150,
                      ),
                      SizedBox(height: 50),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 5.0),
                        child: DropdownButtonFormField(
                          dropdownColor: CustomColors.BackgroundColor,
                          isDense: true,
                          icon: Icon(Icons.arrow_drop_down_circle),
                          iconSize: 25.0,
                          iconEnabledColor: Theme.of(context).primaryColor,
                          items: _danhMucCT.map((String priority) {
                            return DropdownMenuItem(
                              value: priority,
                              child: Text(
                                priority,
                                style: TextStyle(
                                    color: CustomColors.MainLightColor,
                                    fontSize: 15.0),
                              ),
                            );
                          }).toList(),
                          style: TextStyle(fontSize: 18.0),
                          decoration: InputDecoration(
                            labelText: 'Danh mục khoản chi',
                            labelStyle: TextStyle(
                                fontSize: 18.0, color: CustomColors.MPFYellow),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: CustomColors.MainColor,
                                width: 2,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: CustomColors.MainLightColor.withOpacity(
                                    0.5),
                              ),
                            ),
                          ),
                          validator: (input) => _danhMucChiTieu == null
                              ? 'Hãy chọn danh mục khoản chi'
                              : null,
                          onSaved: (input) => _danhMucChiTieu = input,
                          onChanged: (value) {
                            _danhMucChiTieu = value;
                          },
                          value: _danhMucChiTieu,
                        ),
                      ),
                      SizedBox(height: 10),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 5.0),
                        child: TextFormField(
                          readOnly: true,
                          controller: _ngayChiTieuText,
                          style: TextStyle(
                              color: CustomColors.MainLightColor,
                              fontSize: 15.0),
                          onTap: _handleDatePicker1,
                          decoration: InputDecoration(
                            labelText: 'Ngày chi tiêu',
                            labelStyle: TextStyle(
                                color: CustomColors.MPFYellow, fontSize: 18.0),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: CustomColors.MainColor,
                                width: 2,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: CustomColors.MainLightColor.withOpacity(
                                    0.5),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 5.0),
                        child: TextFormField(
                          style: TextStyle(
                              color: CustomColors.MainLightColor,
                              fontSize: 15.0),
                          decoration: InputDecoration(
                            labelText: 'Mô tả',
                            labelStyle: TextStyle(
                                color: CustomColors.MPFYellow, fontSize: 18.0),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: CustomColors.MainColor,
                                width: 2,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: CustomColors.MainLightColor.withOpacity(
                                    0.5),
                              ),
                            ),
                          ),
                          validator: (input) => input.trim().isEmpty
                              ? 'Hãy nhập tmô tả cho chi tiêu'
                              : null,
                          onSaved: (input) => _moTaChiTieu = input,
                          initialValue: _moTaChiTieu,
                        ),
                      ),
                      SizedBox(height: 10),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 5.0),
                        child: TextFormField(
                          style: TextStyle(
                              color: CustomColors.MainLightColor,
                              fontSize: 15.0),
                          decoration: InputDecoration(
                            labelText: 'Số tiền đã sử dụng',
                            labelStyle: TextStyle(
                                color: CustomColors.MPFYellow, fontSize: 18.0),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: CustomColors.MainColor,
                                width: 2,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: CustomColors.MainLightColor.withOpacity(
                                    0.5),
                              ),
                            ),
                          ),
                          validator: (input) =>
                              input.trim().isEmpty ? 'Hãy nhập số tiền' : null,
                          onSaved: (input) => _tongTien = input as double,
                        ),
                      ),
                      SizedBox(height: 20),
                      Row(
                        children: [
                          Material(
                            borderRadius: BorderRadius.circular(100.0),
                            color: CustomColors.MainColor.withOpacity(0.1),
                            child: IconButton(
                              padding: EdgeInsets.all(15.0),
                              icon: Icon(Icons.camera_alt),
                              color: CustomColors.MainColor,
                              iconSize: 30.0,
                              onPressed: () {},
                            ),
                          ),
                          Expanded(
                            child: SizedBox(),
                          ),
                          TextButton(
                            onPressed: () {},
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  CustomColors.MainColor),
                            ),
                            child: Text(
                              'LƯU',
                              style: TextStyle(
                                  fontSize: 18,
                                  color: CustomColors.MainLightColor),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
