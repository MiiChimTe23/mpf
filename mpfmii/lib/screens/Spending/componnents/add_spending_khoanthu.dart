import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mpfmii/constants.dart';

class ADDSpendingKT extends StatefulWidget {
  @override
  _ADDSpendingKTState createState() => _ADDSpendingKTState();
}

class _ADDSpendingKTState extends State<ADDSpendingKT> {
  final _formKey = GlobalKey<FormState>();

  String _danhMucKhoanThu;
  String _moTaKhoanThu = '';
  DateTime _ngayThu = DateTime.now();
  // ignore: unused_field
  double _tongTienThu;

  final DateFormat _dateFormat = DateFormat('dd/MM/yyyy');
  final List<String> _danhMucKT = [
    'Lương',
    'Thưởng/Quà tặng',
    'Thu nợ',
    'Lãi tiếm kiệm',
    'Đầu tư',
    'Khác'
  ];
  TextEditingController _ngayThuText = TextEditingController();

  _handleDatePicker1() async {
    final DateTime date = await showDatePicker(
      context: context,
      initialDate: _ngayThu,
      firstDate: DateTime(2000),
      lastDate: DateTime(2100),
    );
    if (date != null && date != _ngayThu) {
      setState(() {
        _ngayThu = date;
      });
      _ngayThuText.text = _dateFormat.format(date);
    }
  }

  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            size: 30.0,
            color: CustomColors.MainColor,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Text(
          "THÊM KHOẢN THU",
          style: TextStyle(color: CustomColors.MainColor),
        ),
        centerTitle: true,
        backgroundColor: CustomColors.BackgroundColor,
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: SingleChildScrollView(
          child: Container(
            width: double.infinity,
            height: size.height,
            color: CustomColors.BackgroundColor,
            padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 25.0),
            child: Column(
              children: <Widget>[
                Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 20),
                      Image.asset(
                        'assets/images/piggy_bank.png',
                        width: 150,
                        height: 150,
                      ),
                      SizedBox(height: 50),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 5.0),
                        child: DropdownButtonFormField(
                          dropdownColor: CustomColors.BackgroundColor,
                          isDense: true,
                          icon: Icon(Icons.arrow_drop_down_circle),
                          iconSize: 25.0,
                          iconEnabledColor: Theme.of(context).primaryColor,
                          items: _danhMucKT.map((String priority) {
                            return DropdownMenuItem(
                              value: priority,
                              child: Text(
                                priority,
                                style: TextStyle(
                                    color: CustomColors.MainLightColor,
                                    fontSize: 15.0),
                              ),
                            );
                          }).toList(),
                          style: TextStyle(fontSize: 18.0),
                          decoration: InputDecoration(
                            labelText: 'Danh mục khoản thu',
                            labelStyle: TextStyle(
                                fontSize: 18.0, color: CustomColors.MPFYellow),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: CustomColors.MainColor,
                                width: 2,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: CustomColors.MainLightColor.withOpacity(
                                    0.5),
                              ),
                            ),
                          ),
                          validator: (input) => _danhMucKhoanThu == null
                              ? 'Hãy chọn danh mục khoản thu'
                              : null,
                          onSaved: (input) => _danhMucKhoanThu = input,
                          onChanged: (value) {
                            _danhMucKhoanThu = value;
                          },
                          value: _danhMucKhoanThu,
                        ),
                      ),
                      SizedBox(height: 10),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 5.0),
                        child: TextFormField(
                          readOnly: true,
                          controller: _ngayThuText,
                          style: TextStyle(
                              color: CustomColors.MainLightColor,
                              fontSize: 15.0),
                          onTap: _handleDatePicker1,
                          decoration: InputDecoration(
                            labelText: 'Ngày thu',
                            labelStyle: TextStyle(
                                color: CustomColors.MPFYellow, fontSize: 18.0),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: CustomColors.MainColor,
                                width: 2,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: CustomColors.MainLightColor.withOpacity(
                                    0.5),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 5.0),
                        child: TextFormField(
                          style: TextStyle(
                              color: CustomColors.MainLightColor,
                              fontSize: 15.0),
                          decoration: InputDecoration(
                            labelText: 'Mô tả',
                            labelStyle: TextStyle(
                                color: CustomColors.MPFYellow, fontSize: 18.0),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: CustomColors.MainColor,
                                width: 2,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: CustomColors.MainLightColor.withOpacity(
                                    0.5),
                              ),
                            ),
                          ),
                          validator: (input) => input.trim().isEmpty
                              ? 'Hãy nhập tmô tả cho khoản thu'
                              : null,
                          onSaved: (input) => _moTaKhoanThu = input,
                          initialValue: _moTaKhoanThu,
                        ),
                      ),
                      SizedBox(height: 10),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 5.0),
                        child: TextFormField(
                          style: TextStyle(
                              color: CustomColors.MainLightColor,
                              fontSize: 15.0),
                          decoration: InputDecoration(
                            labelText: 'Số tiền đã thu vào',
                            labelStyle: TextStyle(
                                color: CustomColors.MPFYellow, fontSize: 18.0),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: CustomColors.MainColor,
                                width: 2,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: CustomColors.MainLightColor.withOpacity(
                                    0.5),
                              ),
                            ),
                          ),
                          validator: (input) =>
                              input.trim().isEmpty ? 'Hãy nhập số tiền' : null,
                          onSaved: (input) => _tongTienThu = input as double,
                        ),
                      ),
                      SizedBox(height: 20),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          TextButton(
                            onPressed: () {},
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  CustomColors.MainColor),
                            ),
                            child: Text(
                              'LƯU',
                              style: TextStyle(
                                  fontSize: 18,
                                  color: CustomColors.MainLightColor),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
