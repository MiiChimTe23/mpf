import 'package:flutter/material.dart';
import 'package:mpfmii/constants.dart';

class ListViewSpending extends StatefulWidget {
  const ListViewSpending({Key key}) : super(key: key);

  @override
  _ListViewSpendingState createState() => _ListViewSpendingState();
}

class _ListViewSpendingState extends State<ListViewSpending> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.BackgroundColor,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            size: 30.0,
            color: CustomColors.MainColor,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Text(
          "DANH SÁCH KHOẢN CHI",
          style: TextStyle(color: CustomColors.MainColor),
        ),
        centerTitle: true,
        backgroundColor: CustomColors.BackgroundColor,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 25),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 20.0),
                  child: Row(
                    children: <Widget>[
                      Material(
                        borderRadius: BorderRadius.circular(100.0),
                        color: CustomColors.MainLightColor,
                        child: Padding(
                          padding: EdgeInsets.all(5.0),
                          child: Icon(
                            Icons.admin_panel_settings_sharp,
                            color: CustomColors.MainColor,
                            size: 30,
                          ),
                        ),
                      ),
                      SizedBox(width: 15.0),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Mô tả chi tiêu',
                              style: TextStyle(
                                  color: CustomColors.MainColor, fontSize: 15.0, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(height: 5.0),
                            Text(
                              'Số tiền đã chi',
                              style: TextStyle(
                                  color: CustomColors.MainLightColor,
                                  fontSize: 13.0,
                              ),
                            ),
                            Text(
                              'Ngày',
                              style: TextStyle(
                                color: CustomColors.MainLightColor,
                                fontSize: 13.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(
                              icon: Icon(
                                Icons.edit_rounded,
                                color: CustomColors.MPFGreen,
                                size: 25.0,
                              ),
                              onPressed: () {}),
                          IconButton(
                              icon: Icon(
                                Icons.delete_forever,
                                color: CustomColors.MPFRed,
                                size: 25.0,
                              ),
                              onPressed: () {}),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 5.0),
                  child: Row(
                    children: <Widget>[
                      Material(
                        borderRadius: BorderRadius.circular(100.0),
                        color: CustomColors.MainLightColor,
                        child: Padding(
                          padding: EdgeInsets.all(5.0),
                          child: Icon(
                            Icons.admin_panel_settings_sharp,
                            color: CustomColors.MainColor,
                            size: 30,
                          ),
                        ),
                      ),
                      SizedBox(width: 15.0),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Mô tả chi tiêu',
                              style: TextStyle(
                                  color: CustomColors.MainColor, fontSize: 15.0, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(height: 5.0),
                            Text(
                              'Số tiền đã chi',
                              style: TextStyle(
                                color: CustomColors.MainLightColor,
                                fontSize: 13.0,
                              ),
                            ),
                            Text(
                              'Ngày',
                              style: TextStyle(
                                color: CustomColors.MainLightColor,
                                fontSize: 13.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(
                              icon: Icon(
                                Icons.edit_rounded,
                                color: CustomColors.MPFGreen,
                                size: 25.0,
                              ),
                              onPressed: () {}),
                          IconButton(
                              icon: Icon(
                                Icons.delete_forever,
                                color: CustomColors.MPFRed,
                                size: 25.0,
                              ),
                              onPressed: () {}),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 5.0),
                  child: Row(
                    children: <Widget>[
                      Material(
                        borderRadius: BorderRadius.circular(100.0),
                        color: CustomColors.MainLightColor,
                        child: Padding(
                          padding: EdgeInsets.all(5.0),
                          child: Icon(
                            Icons.admin_panel_settings_sharp,
                            color: CustomColors.MainColor,
                            size: 30,
                          ),
                        ),
                      ),
                      SizedBox(width: 15.0),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Mô tả chi tiêu',
                              style: TextStyle(
                                  color: CustomColors.MainColor, fontSize: 15.0, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(height: 5.0),
                            Text(
                              'Số tiền đã chi',
                              style: TextStyle(
                                color: CustomColors.MainLightColor,
                                fontSize: 13.0,
                              ),
                            ),
                            Text(
                              'Ngày',
                              style: TextStyle(
                                color: CustomColors.MainLightColor,
                                fontSize: 13.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(
                              icon: Icon(
                                Icons.edit_rounded,
                                color: CustomColors.MPFGreen,
                                size: 25.0,
                              ),
                              onPressed: () {}),
                          IconButton(
                              icon: Icon(
                                Icons.delete_forever,
                                color: CustomColors.MPFRed,
                                size: 25.0,
                              ),
                              onPressed: () {}),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 5.0),
                  child: Row(
                    children: <Widget>[
                      Material(
                        borderRadius: BorderRadius.circular(100.0),
                        color: CustomColors.MainLightColor,
                        child: Padding(
                          padding: EdgeInsets.all(5.0),
                          child: Icon(
                            Icons.admin_panel_settings_sharp,
                            color: CustomColors.MainColor,
                            size: 30,
                          ),
                        ),
                      ),
                      SizedBox(width: 15.0),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Mô tả chi tiêu',
                              style: TextStyle(
                                  color: CustomColors.MainColor, fontSize: 15.0, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(height: 5.0),
                            Text(
                              'Số tiền đã chi',
                              style: TextStyle(
                                color: CustomColors.MainLightColor,
                                fontSize: 13.0,
                              ),
                            ),
                            Text(
                              'Ngày',
                              style: TextStyle(
                                color: CustomColors.MainLightColor,
                                fontSize: 13.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(
                              icon: Icon(
                                Icons.edit_rounded,
                                color: CustomColors.MPFGreen,
                                size: 25.0,
                              ),
                              onPressed: () {}),
                          IconButton(
                              icon: Icon(
                                Icons.delete_forever,
                                color: CustomColors.MPFRed,
                                size: 25.0,
                              ),
                              onPressed: () {}),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 5.0),
                  child: Row(
                    children: <Widget>[
                      Material(
                        borderRadius: BorderRadius.circular(100.0),
                        color: CustomColors.MainLightColor,
                        child: Padding(
                          padding: EdgeInsets.all(5.0),
                          child: Icon(
                            Icons.admin_panel_settings_sharp,
                            color: CustomColors.MainColor,
                            size: 30,
                          ),
                        ),
                      ),
                      SizedBox(width: 15.0),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Mô tả chi tiêu',
                              style: TextStyle(
                                  color: CustomColors.MainColor, fontSize: 15.0, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(height: 5.0),
                            Text(
                              'Số tiền đã chi',
                              style: TextStyle(
                                color: CustomColors.MainLightColor,
                                fontSize: 13.0,
                              ),
                            ),
                            Text(
                              'Ngày',
                              style: TextStyle(
                                color: CustomColors.MainLightColor,
                                fontSize: 13.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(
                              icon: Icon(
                                Icons.edit_rounded,
                                color: CustomColors.MPFGreen,
                                size: 25.0,
                              ),
                              onPressed: () {}),
                          IconButton(
                              icon: Icon(
                                Icons.delete_forever,
                                color: CustomColors.MPFRed,
                                size: 25.0,
                              ),
                              onPressed: () {}),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 5.0),
                  child: Row(
                    children: <Widget>[
                      Material(
                        borderRadius: BorderRadius.circular(100.0),
                        color: CustomColors.MainLightColor,
                        child: Padding(
                          padding: EdgeInsets.all(5.0),
                          child: Icon(
                            Icons.admin_panel_settings_sharp,
                            color: CustomColors.MainColor,
                            size: 30,
                          ),
                        ),
                      ),
                      SizedBox(width: 15.0),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Mô tả chi tiêu',
                              style: TextStyle(
                                  color: CustomColors.MainColor, fontSize: 15.0, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(height: 5.0),
                            Text(
                              'Số tiền đã chi',
                              style: TextStyle(
                                color: CustomColors.MainLightColor,
                                fontSize: 13.0,
                              ),
                            ),
                            Text(
                              'Ngày',
                              style: TextStyle(
                                color: CustomColors.MainLightColor,
                                fontSize: 13.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(
                              icon: Icon(
                                Icons.edit_rounded,
                                color: CustomColors.MPFGreen,
                                size: 25.0,
                              ),
                              onPressed: () {}),
                          IconButton(
                              icon: Icon(
                                Icons.delete_forever,
                                color: CustomColors.MPFRed,
                                size: 25.0,
                              ),
                              onPressed: () {}),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 5.0),
                  child: Row(
                    children: <Widget>[
                      Material(
                        borderRadius: BorderRadius.circular(100.0),
                        color: CustomColors.MainLightColor,
                        child: Padding(
                          padding: EdgeInsets.all(5.0),
                          child: Icon(
                            Icons.admin_panel_settings_sharp,
                            color: CustomColors.MainColor,
                            size: 30,
                          ),
                        ),
                      ),
                      SizedBox(width: 15.0),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Mô tả chi tiêu',
                              style: TextStyle(
                                  color: CustomColors.MainColor, fontSize: 15.0, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(height: 5.0),
                            Text(
                              'Số tiền đã chi',
                              style: TextStyle(
                                color: CustomColors.MainLightColor,
                                fontSize: 13.0,
                              ),
                            ),
                            Text(
                              'Ngày',
                              style: TextStyle(
                                color: CustomColors.MainLightColor,
                                fontSize: 13.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(
                              icon: Icon(
                                Icons.edit_rounded,
                                color: CustomColors.MPFGreen,
                                size: 25.0,
                              ),
                              onPressed: () {}),
                          IconButton(
                              icon: Icon(
                                Icons.delete_forever,
                                color: CustomColors.MPFRed,
                                size: 25.0,
                              ),
                              onPressed: () {}),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 5.0),
                  child: Row(
                    children: <Widget>[
                      Material(
                        borderRadius: BorderRadius.circular(100.0),
                        color: CustomColors.MainLightColor,
                        child: Padding(
                          padding: EdgeInsets.all(5.0),
                          child: Icon(
                            Icons.admin_panel_settings_sharp,
                            color: CustomColors.MainColor,
                            size: 30,
                          ),
                        ),
                      ),
                      SizedBox(width: 15.0),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Mô tả chi tiêu',
                              style: TextStyle(
                                  color: CustomColors.MainColor, fontSize: 15.0, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(height: 5.0),
                            Text(
                              'Số tiền đã chi',
                              style: TextStyle(
                                color: CustomColors.MainLightColor,
                                fontSize: 13.0,
                              ),
                            ),
                            Text(
                              'Ngày',
                              style: TextStyle(
                                color: CustomColors.MainLightColor,
                                fontSize: 13.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(
                              icon: Icon(
                                Icons.edit_rounded,
                                color: CustomColors.MPFGreen,
                                size: 25.0,
                              ),
                              onPressed: () {}),
                          IconButton(
                              icon: Icon(
                                Icons.delete_forever,
                                color: CustomColors.MPFRed,
                                size: 25.0,
                              ),
                              onPressed: () {}),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 5.0),
                  child: Row(
                    children: <Widget>[
                      Material(
                        borderRadius: BorderRadius.circular(100.0),
                        color: CustomColors.MainLightColor,
                        child: Padding(
                          padding: EdgeInsets.all(5.0),
                          child: Icon(
                            Icons.admin_panel_settings_sharp,
                            color: CustomColors.MainColor,
                            size: 30,
                          ),
                        ),
                      ),
                      SizedBox(width: 15.0),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Mô tả chi tiêu',
                              style: TextStyle(
                                  color: CustomColors.MainColor, fontSize: 15.0, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(height: 5.0),
                            Text(
                              'Số tiền đã chi',
                              style: TextStyle(
                                color: CustomColors.MainLightColor,
                                fontSize: 13.0,
                              ),
                            ),
                            Text(
                              'Ngày',
                              style: TextStyle(
                                color: CustomColors.MainLightColor,
                                fontSize: 13.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(
                              icon: Icon(
                                Icons.edit_rounded,
                                color: CustomColors.MPFGreen,
                                size: 25.0,
                              ),
                              onPressed: () {}),
                          IconButton(
                              icon: Icon(
                                Icons.delete_forever,
                                color: CustomColors.MPFRed,
                                size: 25.0,
                              ),
                              onPressed: () {}),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 5.0),
                  child: Row(
                    children: <Widget>[
                      Material(
                        borderRadius: BorderRadius.circular(100.0),
                        color: CustomColors.MainLightColor,
                        child: Padding(
                          padding: EdgeInsets.all(5.0),
                          child: Icon(
                            Icons.admin_panel_settings_sharp,
                            color: CustomColors.MainColor,
                            size: 30,
                          ),
                        ),
                      ),
                      SizedBox(width: 15.0),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Mô tả chi tiêu',
                              style: TextStyle(
                                  color: CustomColors.MainColor, fontSize: 15.0, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(height: 5.0),
                            Text(
                              'Số tiền đã chi',
                              style: TextStyle(
                                color: CustomColors.MainLightColor,
                                fontSize: 13.0,
                              ),
                            ),
                            Text(
                              'Ngày',
                              style: TextStyle(
                                color: CustomColors.MainLightColor,
                                fontSize: 13.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(
                              icon: Icon(
                                Icons.edit_rounded,
                                color: CustomColors.MPFGreen,
                                size: 25.0,
                              ),
                              onPressed: () {}),
                          IconButton(
                              icon: Icon(
                                Icons.delete_forever,
                                color: CustomColors.MPFRed,
                                size: 25.0,
                              ),
                              onPressed: () {}),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 5.0),
                  child: Row(
                    children: <Widget>[
                      Material(
                        borderRadius: BorderRadius.circular(100.0),
                        color: CustomColors.MainLightColor,
                        child: Padding(
                          padding: EdgeInsets.all(5.0),
                          child: Icon(
                            Icons.admin_panel_settings_sharp,
                            color: CustomColors.MainColor,
                            size: 30,
                          ),
                        ),
                      ),
                      SizedBox(width: 15.0),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Mô tả chi tiêu',
                              style: TextStyle(
                                  color: CustomColors.MainColor, fontSize: 15.0, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(height: 5.0),
                            Text(
                              'Số tiền đã chi',
                              style: TextStyle(
                                color: CustomColors.MainLightColor,
                                fontSize: 13.0,
                              ),
                            ),
                            Text(
                              'Ngày',
                              style: TextStyle(
                                color: CustomColors.MainLightColor,
                                fontSize: 13.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(
                              icon: Icon(
                                Icons.edit_rounded,
                                color: CustomColors.MPFGreen,
                                size: 25.0,
                              ),
                              onPressed: () {}),
                          IconButton(
                              icon: Icon(
                                Icons.delete_forever,
                                color: CustomColors.MPFRed,
                                size: 25.0,
                              ),
                              onPressed: () {}),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 5.0),
                  child: Row(
                    children: <Widget>[
                      Material(
                        borderRadius: BorderRadius.circular(100.0),
                        color: CustomColors.MainLightColor,
                        child: Padding(
                          padding: EdgeInsets.all(5.0),
                          child: Icon(
                            Icons.admin_panel_settings_sharp,
                            color: CustomColors.MainColor,
                            size: 30,
                          ),
                        ),
                      ),
                      SizedBox(width: 15.0),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Mô tả chi tiêu',
                              style: TextStyle(
                                  color: CustomColors.MainColor, fontSize: 15.0, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(height: 5.0),
                            Text(
                              'Số tiền đã chi',
                              style: TextStyle(
                                color: CustomColors.MainLightColor,
                                fontSize: 13.0,
                              ),
                            ),
                            Text(
                              'Ngày',
                              style: TextStyle(
                                color: CustomColors.MainLightColor,
                                fontSize: 13.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(
                              icon: Icon(
                                Icons.edit_rounded,
                                color: CustomColors.MPFGreen,
                                size: 25.0,
                              ),
                              onPressed: () {}),
                          IconButton(
                              icon: Icon(
                                Icons.delete_forever,
                                color: CustomColors.MPFRed,
                                size: 25.0,
                              ),
                              onPressed: () {}),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 5.0),
                  child: Row(
                    children: <Widget>[
                      Material(
                        borderRadius: BorderRadius.circular(100.0),
                        color: CustomColors.MainLightColor,
                        child: Padding(
                          padding: EdgeInsets.all(5.0),
                          child: Icon(
                            Icons.admin_panel_settings_sharp,
                            color: CustomColors.MainColor,
                            size: 30,
                          ),
                        ),
                      ),
                      SizedBox(width: 15.0),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Mô tả chi tiêu',
                              style: TextStyle(
                                  color: CustomColors.MainColor, fontSize: 15.0, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(height: 5.0),
                            Text(
                              'Số tiền đã chi',
                              style: TextStyle(
                                color: CustomColors.MainLightColor,
                                fontSize: 13.0,
                              ),
                            ),
                            Text(
                              'Ngày',
                              style: TextStyle(
                                color: CustomColors.MainLightColor,
                                fontSize: 13.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(
                              icon: Icon(
                                Icons.edit_rounded,
                                color: CustomColors.MPFGreen,
                                size: 25.0,
                              ),
                              onPressed: () {}),
                          IconButton(
                              icon: Icon(
                                Icons.delete_forever,
                                color: CustomColors.MPFRed,
                                size: 25.0,
                              ),
                              onPressed: () {}),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
