import 'package:flutter/material.dart';
import 'package:mpfmii/constants.dart';
import 'package:mpfmii/logic/google_auth.dart';
import 'package:mpfmii/screens/Login/login_screen.dart';
import 'package:mpfmii/screens/Profile/components/about_us.dart';
import 'package:mpfmii/screens/Profile/components/my_acc.dart';
import 'package:mpfmii/screens/Profile/components/notifications.dart';
import 'package:mpfmii/screens/Profile/components/spending_limit.dart';
import 'package:mpfmii/screens/Spending/componnents/body_spending.dart';

class BodyProfiles extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context)
        .size; //bien"size" cung cap chieu cao va chieu rong cua app
    double titleSize = 15;
    //double contentSize = 13;

    return SingleChildScrollView(
      child: Column(
        children: [
          Stack(
            alignment: AlignmentDirectional.topCenter,
            children: <Widget>[
              ClipPath(
                clipper: CustomShapeClipper(),
                child: Container(
                  height: 250.0,
                  decoration: BoxDecoration(color: CustomColors.MainColor),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: size.height * 0.03),
                  Text(
                    'Xin chào nguyenhuong',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: CustomColors.MainLightColor,
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: size.height * 0.01),
                  Image.asset(
                    'assets/images/user.png',
                    width: 200,
                    height: 200,
                  ),
                ],
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            // ignore: deprecated_member_use
            child: FlatButton(
              padding: EdgeInsets.all(20),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15)),
              color: CustomColors.BackgroundColor,
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => MyAccScreen()));
              },
              child: Row(
                children: [
                  Icon(
                    Icons.person,
                    color: CustomColors.MPFPink,
                    size: 30.0,
                  ),
                  SizedBox(width: 20),
                  Expanded(
                    child: Text(
                      'Tài khoản của tôi',
                      style: TextStyle(
                          color: CustomColors.MainColor,
                          fontSize: titleSize,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Icon(
                    Icons.arrow_forward_ios,
                    color: CustomColors.MainLightColor,
                    size: 20.0,
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            // ignore: deprecated_member_use
            child: FlatButton(
              padding: EdgeInsets.all(20),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15)),
              color: CustomColors.BackgroundColor,
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => NotificationMPF()));
              },
              child: Row(
                children: [
                  Icon(
                    Icons.notifications,
                    color: CustomColors.MPFLightBlue,
                    size: 30.0,
                  ),
                  SizedBox(width: 20),
                  Expanded(
                    child: Text(
                      'Thông báo',
                      style: TextStyle(
                          color: CustomColors.MainColor,
                          fontSize: titleSize,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Icon(
                    Icons.arrow_forward_ios,
                    color: CustomColors.MainLightColor,
                    size: 20.0,
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            // ignore: deprecated_member_use
            child: FlatButton(
              padding: EdgeInsets.all(20),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15)),
              color: CustomColors.BackgroundColor,
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => SpendingLimit()));
              },
              child: Row(
                children: [
                  Icon(
                    Icons.settings,
                    color: CustomColors.MPFRed,
                    size: 30.0,
                  ),
                  SizedBox(width: 20),
                  Expanded(
                    child: Text(
                      'Cài đặt giới hạn chi tiêu',
                      style: TextStyle(
                          color: CustomColors.MainColor,
                          fontSize: titleSize,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Icon(
                    Icons.arrow_forward_ios,
                    color: CustomColors.MainLightColor,
                    size: 20.0,
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            // ignore: deprecated_member_use
            child: FlatButton(
              padding: EdgeInsets.all(20),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15)),
              color: CustomColors.BackgroundColor,
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => AboutUsScreen()));
              },
              child: Row(
                children: [
                  Icon(
                    Icons.admin_panel_settings,
                    color: CustomColors.MPFYellow,
                    size: 30.0,
                  ),
                  SizedBox(width: 20),
                  Expanded(
                    child: Text(
                      'Giới thiệu sản phẩm',
                      style: TextStyle(
                          color: CustomColors.MainColor,
                          fontSize: titleSize,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Icon(
                    Icons.arrow_forward_ios,
                    color: CustomColors.MainLightColor,
                    size: 20.0,
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            // ignore: deprecated_member_use
            child: FlatButton(
              padding: EdgeInsets.all(20),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15)),
              color: CustomColors.BackgroundColor,
              onPressed: () {
                signOutGoogle();
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => LoginScreen()));
              },
              child: Row(
                children: [
                  Icon(
                    Icons.logout,
                    color: CustomColors.MPFGreen,
                    size: 30.0,
                  ),
                  SizedBox(width: 20),
                  Expanded(
                    child: Text(
                      'Đăng xuất',
                      style: TextStyle(
                          color: CustomColors.MainColor,
                          fontSize: titleSize,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Icon(
                    Icons.arrow_forward_ios,
                    color: CustomColors.MainLightColor,
                    size: 20.0,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
