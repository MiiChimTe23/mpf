import 'package:flutter/material.dart';
import 'package:mpfmii/constants.dart';

class NotificationMPF extends StatefulWidget {
  const NotificationMPF({Key key}) : super(key: key);

  @override
  _NotificationMPFState createState() => _NotificationMPFState();
}

class _NotificationMPFState extends State<NotificationMPF> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.BackgroundColor,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            size: 30.0,
            color: CustomColors.MainColor,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Text(
          "THÔNG BÁO",
          style: TextStyle(color: CustomColors.MainColor),
        ),
        centerTitle: true,
        backgroundColor: CustomColors.BackgroundColor,
      ),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/images/settings.png',
              width: 100,
              height: 100,
            ),
            SizedBox(height: 50),
            Text(
              'Tính năng đang được phát triển',
              style: TextStyle(fontSize: 20, color: CustomColors.MainLightColor),
            ),
          ],
        ),
      ),
    );
  }
}
