import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mpfmii/constants.dart';

class SpendingLimit extends StatefulWidget {
  const SpendingLimit({Key key}) : super(key: key);

  @override
  _SpendingLimitState createState() => _SpendingLimitState();
}

class _SpendingLimitState extends State<SpendingLimit> {
  final _formKey = GlobalKey<FormState>();

  double _gioiHanChiTieu;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              size: 30.0,
              color: CustomColors.MainLightColor,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          title: Text(
            "GIỚI HẠN CHI TIÊU",
            style: TextStyle(color: CustomColors.MainLightColor),
          ),
          centerTitle: true,
          backgroundColor: CustomColors.BackgroundColor,
        ),
        body: Container(
          width: double.infinity,
          height: size.height,
          decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
              CustomColors.MPFYellow.withOpacity(0.5),
              CustomColors.MainColor.withOpacity(0.9)
            ], begin: Alignment.topRight, end: Alignment.bottomLeft),
          ),
          child: Column(
            children: [
              SizedBox(height: 40),
              Image.asset(
                'assets/images/spending_limit.png',
                width: 150,
                height: 150,
              ),
              Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 30),
                    Padding(
                      padding: EdgeInsets.all(20.0),
                      child: TextFormField(
                        style: TextStyle(color: CustomColors.BackgroundColor, fontSize: 18),
                        decoration: InputDecoration(
                          labelText: 'Giới hạn chi tiêu định mức',
                          labelStyle:
                          TextStyle(color: CustomColors.BackgroundColor),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(
                              color: CustomColors.BackgroundColor,
                              width: 2,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(
                              color: CustomColors.MPFBlue.withOpacity(0.5),
                            ),
                          ),
                        ),
                        validator: (input) =>
                        input.trim().isEmpty ? 'Hãy nhập số tiền' : null,
                        onSaved: (input) => _gioiHanChiTieu = input as double,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 20.0),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          TextButton(
                            onPressed: () {
                            },
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  CustomColors.BackgroundColor),
                            ),
                            child: Text(
                              'CẬP NHẬT',
                              style: TextStyle(
                                  fontSize: 18,
                                  color: CustomColors.MainLightColor),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
