import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mpfmii/constants.dart';
import 'package:mpfmii/screens/Profile/components/background_about_us.dart';
import 'package:mpfmii/screens/Spending/componnents/body_spending.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutUsScreen extends StatelessWidget {
  const AboutUsScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double titleSize = 15;
    return BackgroundAbUs(
      child: SafeArea(
        child: Column(
          children: [
            Row(),
            Stack(
              alignment: AlignmentDirectional.topCenter,
              children: <Widget>[
                ClipPath(
                  clipper: CustomShapeClipper(),
                  child: Container(
                    height: 250.0,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(colors: [
                      Colors.purple.withOpacity(0.9),
                      Colors.pink.withOpacity(0.4)
                    ], begin: Alignment.topLeft, end: Alignment.bottomRight)),
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height: size.height * 0.06),
                    Image.asset(
                      'assets/images/background_aboutus.png',
                      scale: 0.3,
                    ),
                  ],
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              // ignore: deprecated_member_use
              child: FlatButton(
                padding: EdgeInsets.all(20),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15)),
                color: Color(0xFFF5F6F9),
                onPressed: () {},
                child: Row(
                  children: [
                    Icon(
                      Icons.now_widgets,
                      color: Colors.purple,
                      size: 30.0,
                    ),
                    SizedBox(width: 20),
                    Expanded(
                      child: Text(
                        'MPF TTCM: "Quản lý chi tiêu cho bạn"',
                        style: TextStyle(
                            color: CustomColors.MainColor,
                            fontSize: titleSize,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              // ignore: deprecated_member_use
              child: FlatButton(
                padding: EdgeInsets.all(20),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15)),
                color: Color(0xFFF5F6F9),
                onPressed: () {},
                child: Row(
                  children: [
                    Icon(
                      Icons.verified,
                      color: Colors.blue,
                      size: 30.0,
                    ),
                    SizedBox(width: 20),
                    Expanded(
                      child: Text(
                        'Phiên bản 0.0.1',
                        style: TextStyle(
                            color: CustomColors.MainColor,
                            fontSize: titleSize,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              // ignore: deprecated_member_use
              child: FlatButton(
                padding: EdgeInsets.all(20),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15)),
                color: Color(0xFFF5F6F9),
                onPressed: _launchURL,
                child: Row(
                  children: [
                    Icon(
                      Icons.face,
                      color: Colors.red,
                      size: 30.0,
                    ),
                    SizedBox(width: 20),
                    Expanded(
                      child: Text(
                        'Nguyễn Thị Thu Hương',
                        style: TextStyle(
                            color: CustomColors.MainColor,
                            fontSize: titleSize,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              // ignore: deprecated_member_use
              child: FlatButton(
                padding: EdgeInsets.all(20),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15)),
                color: Color(0xFFF5F6F9),
                onPressed: () {},
                child: Row(
                  children: [
                    Icon(
                      Icons.phone_in_talk_sharp,
                      color: Colors.green,
                      size: 30.0,
                    ),
                    SizedBox(width: 20),
                    Expanded(
                      child: Text(
                        '(+84) 909.021.947',
                        style: TextStyle(
                            color: CustomColors.MainColor,
                            fontSize: titleSize,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              // ignore: deprecated_member_use
              child: FlatButton(
                padding: EdgeInsets.all(20),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15)),
                color: Color(0xFFF5F6F9),
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Row(
                  children: [
                    Icon(
                      Icons.arrow_back_ios,
                      color: Colors.yellow,
                      size: 30.0,
                    ),
                    SizedBox(width: 20),
                    Expanded(
                      child: Text(
                        'Quay lại',
                        style: TextStyle(
                            color: CustomColors.MainColor,
                            fontSize: titleSize,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

_launchURL() async {
  const url = 'https://www.facebook.com/mii.utc2.cntt/';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
