import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mpfmii/constants.dart';
import 'package:mpfmii/logic/database.dart';

class MyAccScreen extends StatefulWidget {
  @override
  _MyAccScreenState createState() => _MyAccScreenState();
}

class _MyAccScreenState extends State<MyAccScreen> {
  final _formKey = GlobalKey<FormState>();

  //String _userName = '';
  String _gioiTinh;
  String _hoTen = '';
  DateTime _ngaySinh = DateTime.now();
  // ignore: unused_field

  final DateFormat _dateFormat = DateFormat('dd/MM/yyyy');
  final List<String> _gioiTinhMPF = ['Nam', 'Nữ', 'Khác', 'Không hiển thị'];

  TextEditingController _ngaySinhText = TextEditingController();

  _handleDatePicker1() async {
    final DateTime date = await showDatePicker(
      context: context,
      initialDate: _ngaySinh,
      firstDate: DateTime(1980),
      lastDate: DateTime(2080),
    );
    if (date != null && date != _ngaySinh) {
      setState(() {
        _ngaySinh = date;
      });
      _ngaySinhText.text = _dateFormat.format(date);
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.BackgroundColor,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            size: 30.0,
            color: CustomColors.MainColor,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Text(
          "THÔNG TIN CÁ NHÂN",
          style: TextStyle(color: CustomColors.MainColor),
        ),
        centerTitle: true,
        backgroundColor: CustomColors.BackgroundColor,
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 25.0),
            child: Column(
              children: <Widget>[
                Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 20),
                      Image.asset(
                        'assets/images/user.png',
                        width: 150,
                        height: 150,
                      ),
                      SizedBox(height: 40),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 5.0),
                        child: TextFormField(
                          style: TextStyle(
                              color: CustomColors.MainLightColor, fontSize: 15),
                          decoration: InputDecoration(
                            labelText: 'Họ tên',
                            labelStyle:
                                TextStyle(color: CustomColors.MPFYellow),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: CustomColors.MainColor,
                                width: 2,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: CustomColors.MainLightColor.withOpacity(
                                    0.5),
                              ),
                            ),
                          ),
                          validator: (input) => input.trim().isEmpty
                              ? 'Hãy nhập họ tên của bạn'
                              : null,
                          onSaved: (input) => _hoTen = input,
                          initialValue: _hoTen,
                        ),
                      ),
                      SizedBox(height: 10),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 5.0),
                        child: TextFormField(
                          readOnly: true,
                          controller: _ngaySinhText,
                          style: TextStyle(
                              color: CustomColors.MainLightColor, fontSize: 15),
                          onTap: _handleDatePicker1,
                          decoration: InputDecoration(
                            labelText: 'Ngày sinh',
                            labelStyle:
                                TextStyle(color: CustomColors.MPFYellow),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: CustomColors.MainColor,
                                width: 2,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: CustomColors.MainLightColor.withOpacity(
                                    0.5),
                              ),
                            ),
                          ),
                          validator: (input) => input.trim().isEmpty
                              ? 'Hãy nhập ngày sinh của bạn'
                              : null,
                        ),
                      ),
                      SizedBox(height: 10),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 5.0),
                        child: DropdownButtonFormField(
                          dropdownColor: CustomColors.BackgroundColor,
                          isDense: true,
                          icon: Icon(Icons.arrow_drop_down_circle),
                          iconSize: 22.0,
                          iconEnabledColor: Theme.of(context).primaryColor,
                          items: _gioiTinhMPF.map((String priority) {
                            return DropdownMenuItem(
                              value: priority,
                              child: Text(
                                priority,
                                style: TextStyle(
                                    color: CustomColors.MainLightColor,
                                    fontSize: 15),
                              ),
                            );
                          }).toList(),
                          style: TextStyle(fontSize: 18.0),
                          decoration: InputDecoration(
                            labelText: 'Giới tính',
                            labelStyle:
                                TextStyle(color: CustomColors.MPFYellow),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: CustomColors.MainColor,
                                width: 2,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: CustomColors.MainLightColor.withOpacity(
                                    0.5),
                              ),
                            ),
                          ),
                          validator: (input) =>
                              _gioiTinh == null ? 'Hãy chọn giới tính' : null,
                          onSaved: (input) => _gioiTinh = input,
                          onChanged: (value) {
                            _gioiTinh = value;
                          },
                          value: _gioiTinh,
                        ),
                      ),
                      SizedBox(height: 20),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          TextButton(
                            onPressed: () async {
                              if (_formKey.currentState.validate()) {
                                await Database.updateUser(
                                    fullName: _hoTen,
                                    dateOfBirth: _ngaySinh,
                                    gender: _gioiTinh,
                                    limit: 0.0);
                              }
                            },
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  CustomColors.MainColor),
                            ),
                            child: Text(
                              'CẬP NHẬT',
                              style: TextStyle(
                                  fontSize: 18,
                                  color: CustomColors.MainLightColor),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
