import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mpfmii/constants.dart';
import 'components/profile_body.dart';

class ProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.MainLightColor,
      appBar: AppBar(
        title: Text(
          "THÔNG TIN TÀI KHOẢN",
          style: TextStyle(color: CustomColors.BackgroundColor),
        ),
        centerTitle: true,
        backgroundColor: CustomColors.MainColor,
      ),
      body: BodyProfiles(),
    );
  }
}
