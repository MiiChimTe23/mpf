import 'package:flutter/material.dart';
import 'package:mpfmii/constants.dart';
import 'package:mpfmii/screens/News/news_screen.dart';
import 'package:mpfmii/screens/Profile/profile_screen.dart';
import 'package:mpfmii/screens/Report/report_screen.dart';
import 'package:mpfmii/screens/Spending/spending_sreen.dart';

// ignore: must_be_immutable
class HomeScreenMPF extends StatefulWidget {
  final String displayName;

  double titleSize = 15;
  double contentSize = 13;
  double lableSize = 11;

  HomeScreenMPF({this.displayName});

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreenMPF> {
  int _seletedInex = 0;
  List<IconData> _iconList = [
    Icons.attach_money,
    Icons.bubble_chart_outlined,
    Icons.library_books_outlined,
    Icons.person
  ];

  List<String> _testList = [
    "Chi tiêu",
    "Báo cáo",
    "Tin tức",
    "Tài khoản",
    // Text("Chi tiêu", textAlign: TextAlign.center, ),
    // Text("Báo cáo", textAlign: TextAlign.center,),
    // Text("Tin tức", textAlign: TextAlign.center,),
    // Text("Tài khoản", textAlign: TextAlign.center,),
  ];

  final tabs = [
    // Center(
    //     child: Text("Đây là tab Sổ tiết kiệm",
    //         style: TextStyle(fontSize: 13))),
    Scaffold(
      body: SpendingScreen(),
    ),
    Scaffold(
      body: ReportSrceen(),
    ),
    Scaffold(
      body: NewsScreen(),
    ),
    Scaffold(
      body: ProfileScreen(),
    ),
    // Center(child: Text("Đây là tab thống kê", style: TextStyle(fontSize: 15))),
    // Center(child: Text("Đây là tab tin tức", style: TextStyle(fontSize: 18))),
    // Center(child: Text("Đây là tab tài khoản", style: TextStyle(fontSize: 18))),
  ];

  @override
  Widget build(BuildContext context) {
    List<Widget> _navBarItemList = [];
    for (int i = 0; i < _iconList.length; i++) {
      _navBarItemList.add(buildNavigationItem(_iconList[i], _testList[i], i));
    }

    return Scaffold(
      backgroundColor: CustomColors.BackgroundColor,
      body: tabs[_seletedInex],
      bottomNavigationBar: Row(
        children: _navBarItemList,
      ),
    );
  }

  Widget buildNavigationItem(IconData icon, String label, int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          _seletedInex = index;
        });
      },
      child: Container(
        padding: EdgeInsets.all(7.0),
        child: ListView(
          children: [
            Icon(
              icon,
              size: 20.0,
              color:
                  index == _seletedInex ? Colors.white : CustomColors.MPFYellow,
            ),
            Text(
              label,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: index == _seletedInex
                    ? Colors.white
                    : CustomColors.MPFYellow,
                fontSize: index == _seletedInex ? 12 : 10,
              ),
            )
          ],
        ),
        width: MediaQuery.of(context).size.width / _iconList.length,
        height: 50,
        decoration: index == _seletedInex
            ? BoxDecoration(
                gradient: LinearGradient(colors: [
                CustomColors.MainColor.withOpacity(1.0),
                CustomColors.MPFYellow.withOpacity(0.75)
              ], begin: Alignment.bottomCenter, end: Alignment.topCenter))
            : BoxDecoration(),
      ),
    );
  }
}
