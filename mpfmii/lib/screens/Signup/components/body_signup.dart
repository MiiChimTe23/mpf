import 'package:flutter/material.dart';
import 'package:mpfmii/components/rounded_button.dart';
import 'package:mpfmii/constants.dart';
import 'package:mpfmii/logic/database.dart';
import 'package:mpfmii/logic/validate.dart';
import 'package:mpfmii/screens/Login/login_screen.dart';
import 'package:mpfmii/screens/Signup/components/background_signup.dart';

class BodySI extends StatefulWidget {
  const BodySI({Key key}) : super(key: key);

  @override
  _BodySIState createState() => _BodySIState();
}

class _BodySIState extends State<BodySI> {
  final _formKey = GlobalKey<FormState>();

  bool passwordVisible = true;
  String _userName = '';
  String _password = '';
  DateTime _dateOfBirth = DateTime.now();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: BackgroundSI(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: size.height * 0.03),
              Image.asset(
                "assets/images/logompf.png",
                height: size.height * 0.2,
              ),
              Padding(
                padding: EdgeInsets.all(20.0),
                child: Form(
                  key: _formKey,
                  child: ListView(
                    shrinkWrap: true,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                            left: 5.0, right: 5.0, top: 20.0, bottom: 20.0),
                        child: TextFormField(
                            style: TextStyle(color: CustomColors.MainLightColor,fontSize: 15),
                            autofocus: false,
                            obscureText: false,
                            cursorColor: CustomColors.MPFYellow,
                            decoration: InputDecoration(
                              prefixIcon: Icon(
                                Icons.supervised_user_circle,
                                color: CustomColors.MPFYellow,
                              ),
                              labelText: "Tên tài khoản",
                              labelStyle:
                                  TextStyle(color: CustomColors.MPFYellow),
                              hintText: "Hãy nhập tên tài khoản",
                              hintStyle: TextStyle(
                                  color: CustomColors.MPFGrey.withOpacity(0.5)),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: BorderSide(
                                  color: CustomColors.MainColor,
                                  width: 2,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: BorderSide(
                                  color: CustomColors.MainLightColor.withOpacity(0.5),
                                ),
                              ),
                            ),
                            validator: (value) =>
                                value.isEmpty ? 'Tài khoản không hợp lệ' : null,
                            onChanged: (value) {
                              return _userName = value.trim();
                            }),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            left: 5.0, right: 5.0, top: 10.0, bottom: 10),
                        child: TextFormField(
                          style: TextStyle(color: CustomColors.MainLightColor,fontSize: 15),
                          obscureText: passwordVisible,
                          cursorColor: CustomColors.MPFYellow,
                          decoration: InputDecoration(
                            prefixIcon: Icon(
                              Icons.lock,
                              color: CustomColors.MPFYellow,
                            ),
                            labelText: "Mật khẩu",
                            labelStyle:
                                TextStyle(color: CustomColors.MPFYellow),
                            hintText: "Hãy nhập mật khẩu",
                            hintStyle: TextStyle(
                                color: CustomColors.MPFGrey.withOpacity(0.5)),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: CustomColors.MainColor,
                                width: 2,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: CustomColors.MainLightColor.withOpacity(0.5),
                              ),
                            ),
                            suffixIcon: IconButton(
                                icon: passwordVisible
                                    ? Icon(
                                        Icons.visibility,
                                        color: CustomColors.MPFYellow,
                                      )
                                    : Icon(
                                        Icons.visibility_off,
                                        color: CustomColors.MainColor,
                                      ),
                                onPressed: () {
                                  setState(() {
                                    passwordVisible = !passwordVisible;
                                  });
                                }),
                          ),
                          onChanged: (value) {
                            return _password = value.trim();
                          },
                          validator: (value) {
                            if (!validateStructure(value)) {
                              return 'Vui lòng nhập đúng yêu cầu của mật khẩu';
                            } else {
                              _password = value;
                            }
                            return null;
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              RoundedButton(
                text: "ĐĂNG KÝ",
                press: () async {
                  if (_formKey.currentState.validate()) {
                    await Database.addUser(
                        userName: _userName,
                        password: _password,
                        fullName: '',
                        dateOfBirth: _dateOfBirth,
                        gender: '',
                        limit: 0.0);
                    Database.userUid = _userName;
                    Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (context) => LoginScreen()));
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
