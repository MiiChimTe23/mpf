import 'package:flutter/material.dart';
import 'package:mpfmii/constants.dart';

class SocalIcon extends StatelessWidget {
  final String icon;
  final Function press;
  const SocalIcon({
    Key key,
    this.icon,
    this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
        padding: EdgeInsets.all(20),
        decoration: BoxDecoration(
          border: Border.all(
            width: 2,
            color: CustomColors.MainLightColor,
          ),
          shape: BoxShape.circle,
        ),
        child: Image.asset(
          icon,
          height: 20,
          width: 20,
        ),
      ),
    );
  }
}
