import 'package:flutter/material.dart';
import 'package:mpfmii/screens/Signup/components/background_signup.dart';
import 'package:mpfmii/screens/Signup/components/body_signup.dart';

class SignUpScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BackgroundSI(
      child: Scaffold(
        body: BodySI(),
      ),
    );
  }
}
