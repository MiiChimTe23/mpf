import 'package:flutter/material.dart';
import 'package:mpfmii/screens/Login/components/background_login.dart';
import 'package:mpfmii/screens/Login/components/body_login.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BackgroundLG(
      child: Scaffold(
        body: BodyLG(),
      ),
    );
  }
}
