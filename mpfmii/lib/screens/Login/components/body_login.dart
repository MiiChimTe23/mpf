import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:mpfmii/components/already_have_an_account_acheck.dart';
import 'package:mpfmii/components/rounded_button.dart';
import 'package:mpfmii/constants.dart';
import 'package:mpfmii/logic/google_auth.dart';
import 'package:mpfmii/logic/validate.dart';
import 'package:mpfmii/screens/Home/home_screen.dart';
import 'package:mpfmii/screens/Login/components/background_login.dart';
import 'package:mpfmii/screens/Login/components/or_divider.dart';
import 'package:mpfmii/screens/Signup/components/social_icon.dart';
import 'package:mpfmii/screens/Signup/signup_screen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class BodyLG extends StatefulWidget {
  const BodyLG({Key key}) : super(key: key);

  @override
  _BodyLGState createState() => _BodyLGState();
}

class _BodyLGState extends State<BodyLG> {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  final CollectionReference usersCollection =
      FirebaseFirestore.instance.collection('appmmpf');

  bool passwordVisible = true;
  final _formKey = GlobalKey<FormState>();
  String _userName = '';
  String _password = '';
  bool correctUserName;
  bool correctPassword;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: BackgroundLG(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: size.height * 0.03),
              Image.asset(
                "assets/images/logompf.png",
                height: size.height * 0.2,
              ),
              SizedBox(height: size.height * 0.03),
              Padding(
                padding: EdgeInsets.all(20.0),
                child: Form(
                  key: _formKey,
                  child: ListView(
                    shrinkWrap: true,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                            left: 5.0, right: 5.0, top: 20.0, bottom: 20.0),
                        child: TextFormField(
                          style: TextStyle(color: CustomColors.MainLightColor, fontSize: 15),
                            autofocus: false,
                            obscureText: false,
                            cursorColor: CustomColors.MPFYellow,
                            decoration: InputDecoration(
                              prefixIcon: Icon(
                                Icons.supervised_user_circle,
                                color: CustomColors.MPFYellow,
                              ),
                              labelText: "Tên tài khoản",
                              labelStyle:
                                  TextStyle(color: CustomColors.MPFYellow),
                              hintText: "Hãy nhập tên tài khoản",
                              hintStyle: TextStyle(
                                  color: CustomColors.MPFGrey.withOpacity(0.5)),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: BorderSide(
                                  color: CustomColors.MainColor,
                                  width: 2,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: BorderSide(
                                  color: CustomColors.MainLightColor.withOpacity(0.5),
                                ),
                              ),
                            ),
                            validator: (value) =>
                                value.isEmpty ? 'Tài khoản không hợp lệ' : null,
                            onChanged: (value) {
                              return _userName = value;
                            }),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            left: 5.0, right: 5.0, top: 10.0, bottom: 10),
                        child: TextFormField(
                          style: TextStyle(color: CustomColors.MainLightColor, fontSize: 15),
                          obscureText: passwordVisible,
                          cursorColor: CustomColors.MPFYellow,
                          decoration: InputDecoration(
                            prefixIcon: Icon(
                              Icons.lock,
                              color: CustomColors.MPFYellow,
                            ),
                            labelText: "Mật khẩu",
                            labelStyle:
                                TextStyle(color: CustomColors.MPFYellow),
                            hintText: "Hãy nhập mật khẩu",
                            hintStyle: TextStyle(
                                color: CustomColors.MPFGrey.withOpacity(0.5)),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: CustomColors.MainColor,
                                width: 2,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: CustomColors.MainLightColor.withOpacity(0.5),
                              ),
                            ),
                            suffixIcon: IconButton(
                                icon: passwordVisible
                                    ? Icon(
                                        Icons.visibility,
                                        color: CustomColors.MPFYellow,
                                      )
                                    : Icon(
                                        Icons.visibility_off,
                                        color: CustomColors.MainColor,
                                      ),
                                onPressed: () {
                                  setState(() {
                                    passwordVisible = !passwordVisible;
                                  });
                                }),
                          ),
                          onChanged: (value) {
                            return _password = value;
                          },
                          validator: (value) {
                            if (!validateStructure(value)) {
                              return 'Vui lòng nhập đúng yêu cầu của mật khẩu';
                            } else {
                              _password = value;
                            }
                            return null;
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              RoundedButton(
                text: "ĐĂNG NHẬP",
                press: () {
                  usersCollection
                      .where("user_name", isEqualTo: _userName)
                      .get()
                      .then((QuerySnapshot querySnapshot) {
                    querySnapshot.docs.forEach((doc) {
                      correctUserName = true;
                      if (correctUserName) {
                        print(doc["user_name"]);
                      }
                    });
                  });

                  usersCollection
                      .where("password", isEqualTo: _password)
                      .get()
                      .then((QuerySnapshot querySnapshot) {
                    querySnapshot.docs.forEach((doc) {
                      correctPassword = true;
                      if (correctPassword) {
                        print(doc["password"]);
                      }
                    });
                  });
                  if (correctUserName && correctPassword) {
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (context) => HomeScreenMPF()));
                  }
                },
              ),
              SizedBox(height: size.height * 0.03),
              AlreadyHaveAnAccountCheck(
                press: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return SignUpScreen();
                      },
                    ),
                  );
                },
              ),
              OrDivider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SocalIcon(
                    icon: "assets/images/google_plus.png",
                    press: () {
                      signInWithGoogle().then((value) {
                        if (value != null) {
                          Navigator.of(context)
                               .pushReplacement(MaterialPageRoute(
                                  builder: (context) => HomeScreenMPF(
                                        displayName:
                                            _auth.currentUser.displayName,
                                      )));
                        }
                      });
                    },
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

// _db
//     .collection("activities")
// .add({'first': "Bda", 'last': "Lovelace", 'born': 1815}).then(
// (docRef) =>
// {print("Document written with ID: ${docRef.id}")});
// _db
//     .collection('activities')
// .where('first', isEqualTo: "Ada")
// .get()
//     .then((QuerySnapshot querySnapshot) {
// querySnapshot.docs.forEach((doc) {
// print('${doc.id} => ${doc.data()}');
// });
// });
