import 'package:flutter/material.dart';
import 'package:mpfmii/constants.dart';
import 'package:mpfmii/screens/Welcome/components/background_welcome.dart';

class BodyWC extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context)
        .size; //bien"size" cung cap chieu cao va chieu rong cua app

    return BackgroundWC(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Image.asset(
              "assets/images/logompf.png",
              height: size.height * 0.3,
            ),
            SizedBox(height: size.height * 0.03),
            Text(
              'Quản lý chi tiêu',
              style: TextStyle(
                color: CustomColors.MainColor,
                fontSize: 40,
              ),
            ),
            Text(
              'MPF',
              style: TextStyle(
                color: CustomColors.MPFYellow,
                fontSize: 40,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
