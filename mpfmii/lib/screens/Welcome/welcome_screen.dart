import 'dart:async';
import 'package:flutter/material.dart';
import 'package:mpfmii/screens/Login/login_screen.dart';
import 'package:mpfmii/screens/Welcome/components/background_welcome.dart';
import 'package:mpfmii/screens/Welcome/components/body_welcome.dart';

class Welcome extends StatefulWidget {
  const Welcome({Key key}) : super(key: key);

  @override
  _WelcomeState createState() => _WelcomeState();
}

class _WelcomeState extends State<Welcome> {
  @override
  Widget build(BuildContext context) {
    return BackgroundWC(
      child: Scaffold(
        body: BodyWC(),
      ),
    );
  }

  void navigateToLogin() {
    Timer(Duration(seconds: 5), () {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) {
            return LoginScreen();
          },
        ),
      );
    });
  }

  @override
  void initState() {
    super.initState();
    navigateToLogin();
  }
}
