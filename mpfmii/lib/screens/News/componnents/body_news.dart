import 'package:flutter/material.dart';
import 'package:flutter_image_slideshow/flutter_image_slideshow.dart';
import 'package:mpfmii/constants.dart';

class BodyNews extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //bien"size" cung cap chieu cao va chieu rong cua app
    double titleSize = 15;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        ImageSlideshow(
          width: double.infinity,
          height: 200,
          initialPage: 0,
          indicatorColor: CustomColors.MainColor,
          indicatorBackgroundColor: CustomColors.MainLightColor,
          children: [
            Image.asset(
              'assets/images/slide_one.png',
              fit: BoxFit.cover,
            ),
            Image.asset(
              'assets/images/slide_two.png',
              fit: BoxFit.cover,
            ),
            Image.asset(
              'assets/images/slide_three.png',
              fit: BoxFit.cover,
            ),
          ],
          onPageChanged: (value) {},
          autoPlayInterval: 10000,
        ),
        Container(
          width: double.infinity,
          color: CustomColors.BackgroundColor,
          padding: EdgeInsets.symmetric(vertical: 15),
          child: Text("TIN TỨC TÀI CHÍNH",
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: CustomColors.MainLightColor,
                  fontSize: titleSize * 1.5,
                  fontWeight: FontWeight.bold)),
        ),
      ],
    );
  }
}
