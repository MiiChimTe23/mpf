import 'package:flutter/material.dart';
import 'package:mpfmii/constants.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:web_scraper/web_scraper.dart';

class NewsList extends StatefulWidget {
  @override
  _NewsListState createState() => _NewsListState();
}

class ThongBao {
  String tieude;
  String link;

  ThongBao(String tieude, String link) {
    this.tieude = tieude;
    this.link = link;
  }
}

class _NewsListState extends State<NewsList> {
  final webScraper = WebScraper('https://vnexpress.net');

  List<Map<String, dynamic>> tieuDeThongBao;
  List<ThongBao> dsThongBao;

  void fetchProducts() async {
    if (await webScraper.loadWebPage('/kinh-doanh/tien-cua-toi')) {
      setState(() {
        tieuDeThongBao = webScraper.getElement(
            'article.item-news.item-news-common > h2.title-news > a',
            ['href', 'title']);

        ThongBao thongBao;
        dsThongBao = [];
        Map<String, dynamic> attributes;
        for (int i = 0; i < tieuDeThongBao.length; i++) {
          attributes = tieuDeThongBao[i]['attributes'];
          thongBao =
              new ThongBao(tieuDeThongBao[i]['title'], attributes['href']);
          dsThongBao.add(thongBao);
        }
      });
    }
  }

  @override
  void initState() {
    super.initState();
    fetchProducts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.BackgroundColor,
      body: tieuDeThongBao == null
          ? Center(
              child:
                  CircularProgressIndicator(), // Loads Circular Loading Animation
            )
          : ListView.builder(
              itemCount: tieuDeThongBao.length,
              itemBuilder: (BuildContext context, int index) {
                // Attributes are in the form of List<Map<String, dynamic>>.
                Map<String, dynamic> attributes =
                    tieuDeThongBao[index]['attributes'];

                return Card(
                  color: CustomColors.MainLightColor.withOpacity(0.9),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(bottom: 20.0),
                          child: InkWell(
                            onTap: () {
                              // uses UI Launcher to launch in web browser & minor tweaks to generate url
                              launch(attributes['href']);
                            },
                            child: Text(
                              tieuDeThongBao[index]['title'],
                              style: TextStyle(
                                color: CustomColors.BackgroundColor,
                                fontSize: 15, 
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
    );
  }
}
