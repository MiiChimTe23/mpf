import 'package:flutter/material.dart';
import 'package:mpfmii/screens/News/componnents/body_news.dart';
import 'package:mpfmii/screens/News/componnents/news_list.dart';

class NewsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Color.fromRGBO(244, 244, 244, 1),
      body: SafeArea(
        child: Column(
          children: [
            BodyNews(),
            Expanded(child: NewsList()),
          ],
        ),
      ),
    );
  }
}
