import 'package:flutter/material.dart';

class CustomColors {
  static final Color MainColor = Color(0xFFF57C00);
  static final Color MainLightColor = Color(0xFFECEFF1);
  static final Color BackgroundColor = Color(0xFF2C384A);
  static final Color MPFGreen = Color(0xFF0F7F03);
  static final Color MPFBlue = Color(0xFF0938C6);
  static final Color MPFPurple = Color(0xFF6C037F);
  static final Color MPFYellow = Color(0xFFFFCA28);
  static final Color MPFRed = Color(0xFFEA0707);
  static final Color MPFLightBlue = Color(0xFF4285F4);
  static final Color MPFPink = Color(0xFFFF00D3);
  static final Color MPFGrey = Color(0xFF787577);
}
