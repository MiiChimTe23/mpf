import 'package:flutter/material.dart';
import 'package:mpfmii/constants.dart';

class RoundedButton extends StatelessWidget {
  final String text;
  final Function press;
  final Color color, textColor;

  const RoundedButton({
    Key key,
    this.text,
    this.press,
    this.color,
    this.textColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      width: size.width * 0.5,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        // ignore: deprecated_member_use
        child: FlatButton(
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
          color: CustomColors.MainColor,
          onPressed: press,
          child: Text(
            text,
            style: TextStyle(color: CustomColors.MainLightColor, fontSize: 20),
          ),
        ),
      ),
    );
  }
}
