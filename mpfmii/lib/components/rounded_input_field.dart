import 'package:flutter/material.dart';
import 'package:mpfmii/components/text_field_container.dart';
import 'package:mpfmii/constants.dart';

class RoundedInputField extends StatelessWidget {
  final String lableText;
  final String hintText;
  final IconData icon;
  final ValueChanged<String> onChanged;
  const RoundedInputField({
    Key key,
    this.lableText,
    this.hintText,
    this.icon = Icons.person,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextFormField(
        onChanged: onChanged,
        cursorColor: CustomColors.MainColor,
        decoration: InputDecoration(
          icon: Icon(
            icon,
            color: CustomColors.MainColor,
          ),
          labelText: lableText,
          hintText: hintText,
          border: InputBorder.none,
        ),
      ),
    );
  }
}
