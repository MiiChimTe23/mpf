import 'package:flutter/material.dart';
import 'package:mpfmii/constants.dart';

class RoundedButtonText extends StatelessWidget {
  final String text;
  final double titleSize = 15;
  final double contentSize = 13;
  final double lableSize = 11;
  final Function press;
  final Color color, textColor;

  const RoundedButtonText({
    Key key,
    this.text,
    this.press,
    this.color,
    this.textColor = Colors.black,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      width: size.width * 0.3,
      height: size.height * 0.08,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        // ignore: deprecated_member_use
        child: FlatButton(
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
          color: color,
          onPressed: press,
          child: Text(
            text,
            style: TextStyle(color: textColor, fontSize: titleSize),
          ),
        ),
      ),
    );
  }
}
