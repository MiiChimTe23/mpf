import 'package:flutter/material.dart';
import 'package:mpfmii/components/text_field_container.dart';
import 'package:mpfmii/constants.dart';

class RoundedPasswordField extends StatefulWidget {
  @override
  _RoundedPasswordFieldState createState() => _RoundedPasswordFieldState();
}

class _RoundedPasswordFieldState extends State<RoundedPasswordField> {
  bool passwordVisible = true;
  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextFormField(
        obscureText: passwordVisible,
        //onChanged: (){},
        cursorColor: CustomColors.MainColor,
        decoration: InputDecoration(
          labelText: "Mật khẩu",
          hintText: "Hãy nhập mật khẩu",
          border: InputBorder.none,
          icon: Icon(
            Icons.lock,
            color: CustomColors.MainColor,
          ),
          suffixIcon: IconButton(
              icon: passwordVisible
                  ? Icon(
                      Icons.visibility,
                      color: CustomColors.MainColor,
                    )
                  : Icon(
                      Icons.visibility_off,
                      color: CustomColors.MainColor,
                    ),
              onPressed: () {
                setState(() {
                  passwordVisible = !passwordVisible;
                });
              }),
        ),
      ),
    );
  }
}
